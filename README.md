## Student Author and Contact Info
Newton Blair 
email: nblair@uoregon.edu


## Software Description
This software replicates the ACP brevet open and close time calculator. With it you are able to select a start date, time and a desired overall brevet distance. After selecting these you may begin to type in checkpoint distances in either miles or km and upon clicking on the next block it will autogenerate the open and close time for that checkpoint distance based on the start time and date. These times are generated based on a set of specifications of which I will specify below. Once finished adding your control points click on the submit button, and then the display button to be direct to a page that lists your control points and your open and close times for you.

## Specifications
1)the closing of the starting checkpoint (0km) is always 1 hour after the specified start time.

2)the speeds used for the checkpoints are broken up by the distances and speeds specified by the chart found on the following brevets calculation website: (https://rusa.org/pages/acp-brevet-control-times-calculator)

		-This means if a checkpoint is at 200km or less the open and close time is calculated as: 
		open time = start time + distance(km)/(max speed for 0-200)
		close time = start time + distance(km)/(min speed for 0-200)

		-if a checkpoint is located at 750km it is broken up into 4 parts:
		open time = start time + (200/34) + (200/32) + (200/30) + (150/28)
		clsoe time = start time + (200/15) + (200/15) + (200/15) + (150/11.428)

3) There are set closing times for the last checkpoint of a race given a specified time these times are found on this brevets rule webpage:https://rusa.org/pages/orgreg
		
		-This means that any checkpoint placed at or after the specified distance is calculated using the specified close time given from the website. There are however a few things to note:

			1)Only the closing time is calculated using the specified times for these instances

			1)distances less than the specified overall brevet distance are calculated normally both for open and close times

			2)Any checkpoint distance exceeding the overall specified distance of the race is treated as if it was placed at the overall specified distance unless it is 1.2 times the overall distance in which case an error will be displayed at the top of the page saying that the checkpoint is set too far for a race of the specified distance 

## Notes
1) Pressing submit on a form with no control points will take you to a error page asking you to fix that mistake with a button to take you back to the page.

2) Pressing on the display button before the submit button will take you to the 'finished' page, but had a note saying if this page is empty you did not hit submit and should go back and try again with a button that takes you back to the starting page. 