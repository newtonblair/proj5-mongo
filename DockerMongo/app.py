import os
import flask
from flask import Flask, redirect, url_for, request, render_template
from pymongo import MongoClient
####
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
import logging
import requests

###
# Globals
###
app = Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY


client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb


@app.route('/done', methods=["POST"])
def todo():
    _items = db.tododb.find()
    items = [item for item in _items]
    db.tododb.remove({})
    return flask.render_template('todo.html', items=items)

@app.route('/new', methods=['POST'])
def new():
    km_vals = []
    open_vals = []
    close_vals = []
    mult = request.form
    for val in mult.getlist('km'):
        if val != '':
            print("still looping")
            km_vals.append(val)
        else:
            break
    length = len(km_vals)
    if length == 0:
        return flask.render_template('empty_form.html')
    else:
        needed = {'open':open_vals, 'close':close_vals}
        last_keys = list(needed.keys())
        for key in last_keys:
            for val in mult.getlist(key):
                if val != '':
                    selectList = needed[key]
                    selectList.append(val)
                else:
                    break
        for i in range(length):
            item_doc = {
                'distance': request.form['distance'],
                'km': km_vals[i],
                'open': open_vals[i],
                'close': close_vals[i]
            }
            app.logger.debug("Here is an extracted val: {} ".format(item_doc))
            db.tododb.insert_one(item_doc)
    return redirect(url_for("index"))


######Stuff from flask_brevets

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')                           #Renders the main page for the website


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")                  #Attempts to re-render the main page
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    begin_time = request.args.get('begin_time')                      #grabs the begin_time data from html page
    begin_date = request.args.get('begin_date')                      #grabs the begin_date data from html page
    distance = float(request.args.get('distance'))                   #grabs the distance data from html page
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    if km >= (1.2*distance):                                         #if checkpoint is 20% larger than distance => error
        result = {"error":-1}
        return flask.jsonify(result=result)                          #return error to raise error code
    start_date = begin_date+" "+begin_time                           #formats date and time into a string to be passed into arrow 
    # FIXME: These probably aren't the right open and close times
    # and brevets may be longer than 200km
    open_time = acp_times.open_time(km, distance, start_date)        #solve for open time of checkpoint
    close_time = acp_times.close_time(km, distance, start_date)      #solve for close time of checkpoint
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
